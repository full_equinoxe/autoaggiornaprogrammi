
# TODO: Per Windows servono i pacchetti WGET, CURL e GREP
clear
echo "-----------------------------------------"
echo "      Programma di aggiornamento         "
echo "    dei file di installazione per        "
echo "      Windows 32-bit e 64-bit            "
echo "                v1.0                     "
echo "    Realizzato da Giovanni Montini       "
echo "        <giovimonto@linux.it>            "
echo "-----------------------------------------"
echo ""
echo "Creazione delle cartelle..."
mkdir -p 32bit
mkdir -p 64bit
echo "Cartelle create"
echo "Recupero l'ultimo pacchetto di installazione di Google Chrome (32-bit)"
echo "Passo 1 di 15 "
wget -q "https://dl.google.com/tag/s/appguid%3D%7B8A69D345-D564-463C-AFF1-A69D9E530F96%7D%26iid%3D%7B161FABFA-3904-6E06-54F8-62D13EDA901E%7D%26lang%3Dit%26browser%3D3%26usagestats%3D0%26appname%3DGoogle%2520Chrome%26needsadmin%3Dprefers%26ap%3Dstable-arch_x86-statsdef_1%26installdataindex%3Dempty/chrome/install/ChromeStandaloneSetup.exe" -O 32bit/GoogleChrome_32.exe

echo "Recupero l'ultimo pacchetto di installazione di Google Chrome (64-bit)"
echo "Passo 2 di 15 "
wget -q "https://dl.google.com/tag/s/appguid%3D%7B8A69D345-D564-463C-AFF1-A69D9E530F96%7D%26iid%3D%7B161FABFA-3904-6E06-54F8-62D13EDA901E%7D%26lang%3Dit%26browser%3D3%26usagestats%3D0%26appname%3DGoogle%2520Chrome%26needsadmin%3Dprefers%26ap%3Dx64-stable-statsdef_1%26installdataindex%3Dempty/chrome/install/ChromeStandaloneSetup64.exe" -O 64bit/GoogleChrome_64.exe

echo "Recupero l'ultimo pacchetto di installazione di Mozilla Firefox (32-bit)"
echo "Passo 3 di 15 "
wget -q "https://download.mozilla.org/?product=firefox-latest&os=win&lang=it" -O 32bit/Firefox_32.exe

echo "Recupero l'ultimo pacchetto di installazione di Mozilla Firefox (64-bit)"
echo "Passo 4 di 15 "
wget -q "https://download.mozilla.org/?product=firefox-latest&os=win64&lang=it" -O 64bit/Firefox_64.exe

echo "Recupero l'ultimo pacchetto di installazione di Java (32-bit)"
echo "Passo 5 di 15 "
wget -q "https://javadl.oracle.com/webapps/download/AutoDL?BundleId=238727_478a62b7d4e34b78b671c754eaaf38ab" -O 32bit/Java_32.exe

echo "Recupero l'ultimo pacchetto di installazione di Java (64-bit)"
echo "Passo 6 di 15 "
wget -q "https://javadl.oracle.com/webapps/download/AutoDL?BundleId=238729_478a62b7d4e34b78b671c754eaaf38ab" -O 64bit/Java_64.exe

# LIBREOFFICE È DA FARE!!!
# LINK GENERICO https://libreoffice.mirror.garr.it/mirrors/tdf/libreoffice/stable/
# Scrivi uno script che riconosca il valore della versione più alto
# e poi fai in modo che scarichi da li

#echo "Recupero l'ultimo pacchetto di installazione di LibreOffice (32-bit)"
#echo "Passo 7 di 15 "
#wget -q "https://it.libreoffice.org/download/libreoffice-fresh/?type=win-x86&version=6.2&lang=it" -O 32bit/Libreoffice_32.exe

#echo "Recupero l'ultimo pacchetto di installazione di LibreOffice (64-bit)"
#echo "Passo 8 di 15 "
#wget -q "https://it.libreoffice.org/download/libreoffice-fresh/?type=win-x86_64&version=6.2&lang=it" -O 64bit/Libreoffice_64.exe

echo "Recupero l'ultimo pacchetto di installazione di Everything (32-bit)"
echo "Passo 9 di 15 "
EVERYTHING32=$(curl -s https://voidtools.com | grep -o '/Everything.*86.*.exe')
EVERYTHING32K=http://voidtools.com$EVERYTHING32
wget -q $EVERYTHING32K -O 32bit/Everything_32.exe

echo "Recupero l'ultimo pacchetto di installazione di Everything (64-bit)"
echo "Passo 10 di 15 "
EVERYTHING64=$(curl -s https://voidtools.com | grep -o '/Everything.*64.*.exe')
EVERYTHING64K=http://voidtools.com$EVERYTHING64
wget -q $EVERYTHING64K -O 64bit/Everything_64.exe

echo "Recupero l'ultimo pacchetto di installazione di Mozilla Thunderbird (32-bit)"
echo "Passo 11 di 15 "
wget -q "https://download.mozilla.org/?product=thunderbird-60.6.1-SSL&os=win&lang=it" -O 32bit/Thunderbird_32.exe

echo "Recupero l'ultimo pacchetto di installazione di Mozilla Thunderbird (64-bit)"
echo "Passo 13 di 15 "
wget -q "https://download.mozilla.org/?product=thunderbird-60.6.1-SSL&os=win64&lang=it" -O 64bit/Thunderbird_64.exe

echo "Recupero l'ultimo pacchetto di installazione di VLC Media Player (32-bit)"
echo "Passo 14 di 15 "
VLC32=$(curl -s http://download.videolan.org/pub/videolan/vlc/last/win32/ | grep -o 'vlc.*32.exe\"' | sed 's/.$//')
VLC32K=http://download.videolan.org/pub/videolan/vlc/last/win32/$VLC32
wget -q $VLC32K -O 32bit/VLC_32.exe

echo "Recupero l'ultimo pacchetto di installazione di VLC Media Player (64-bit)"
echo "Passo 15 di 15 "
VLC64=$(curl -s http://download.videolan.org/pub/videolan/vlc/last/win64/ | grep -o 'vlc.*64.exe\"' | sed 's/.$//')
VLC64K=http://download.videolan.org/pub/videolan/vlc/last/win32/$VLC64
wget -q "$VLC64K" -O 64bit/VLC_64.exe
